from django.conf.urls import url
from .views import index, add_todo
app_name='lab-5'
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_todo', add_todo, name='add_todo'),
]