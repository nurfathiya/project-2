from django.conf.urls import url
from .views import index
app_name='lab-6'
urlpatterns = [
    url(r'^$', index, name='index'),
]